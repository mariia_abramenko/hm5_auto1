﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyCalcLib;

namespace Calculator1.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Add_5and10_15return()
        {
            //arrange
            int x = 5;
            int y = 10;

            int expected = 15;
            //act
            MyCalc c = new MyCalc();
            int actual = c.Add(x, y);

            //assert

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Subs_10and5_5return()
            {
            //arrange
            int x = 10;
            int y = 5;
            int expected = 5;
            //act
            MyCalc c = new MyCalc();
            int actual = c.Subs(x, y);

            //assert

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Divide_10and5_2return()
        {

            //arrange
            int x = 10;
            int y = 5;
            int expected = 2;
            //act
            MyCalc c = new MyCalc();
            int actual = c.Divide(x, y);

            //assert

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Multiply_2and10_20return()
        {

            //arrange
            int x = 2;
            int y = 10;
            int expected = 20;
            //act
            MyCalc c = new MyCalc();
            int actual = c.Multiply(x, y);

            //assert

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Add_10and10_return20()
        {

            //arrange
            int x = 10;
            int y = 10;
            int expected = 2;
            //act
            MyCalc c = new MyCalc();
            int actual = c.Add(x, y);

            //assert

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Subs_10and2_return8()
        {

            //arrange
            int x = 10;
            int y = 2;
            int expected = 5;
            //act
            MyCalc c = new MyCalc();
            int actual = c.Add(x, y);

            //assert

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Divide_10and2_5return()
        {

            //arrange
            int x = 10;
            int y = 2;
            int expected = 2;
            //act
            MyCalc c = new MyCalc();
            int actual = c.Divide(x, y);

            //assert

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Multiply_2and5_10return()
        {

            //arrange
            int x = 2;
            int y = 5;
            int expected = 10;
            //act
            MyCalc c = new MyCalc();
            int actual = c.Multiply(x, y);

            //assert

            Assert.AreNotEqual(expected, actual);
        }
    }
}
